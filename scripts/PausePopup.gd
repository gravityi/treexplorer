extends PopupMenu

func _ready():
	pass

func _process(delta):
	if Input.is_action_just_pressed("pause"):
		get_tree().paused = true
		popup_centered()

func unpause():
	get_tree().paused = false

func _on_PausePopup_id_pressed(id):
	match id:
		0:
			unpause()
			hide()
		1:
			unpause()
			Transition.fade_to_scene("res://MainMenu.tscn")
		2:
			get_tree().quit()
