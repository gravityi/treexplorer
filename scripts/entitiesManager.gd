extends Node

const Player = preload("res://player/Player.tscn")
const enemy = preload("res://enemy/Enemy.tscn")
const Portal = preload("res://assets/Portal.tscn")

func spawn_player(pos):
	var NewPlayer = Player.instance()
	NewPlayer.translation = pos
	add_child(NewPlayer)

func spawn_portal(pos):
	var NewPortal = Portal.instance()
	NewPortal.translation = pos
	add_child(NewPortal)

func _on_map_generated(player_spawn_position, portal_spawn_position, enemy_positions):
	spawn_player(player_spawn_position)
	spawn_portal(portal_spawn_position)
	$"EnemySpawner".on_dungeon_generated(enemy_positions)

