extends VBoxContainer

func _on_Start_pressed():
	Transition.fade_to_scene("res://levels/RandomDungeon.tscn")
	Globals.health = 5
	Globals.gauge_value = 0


func _on_Tutorial_pressed():
	Transition.fade_to_scene("res://levels/PassiveSelection.tscn")


func _on_Settings_pressed():
	$"Settings/SettingsPopup".popup_centered()


func _on_Quit_pressed():
	get_tree().quit()
