extends Area

func _on_Area_body_entered(body):
	if body.is_in_group("player"):
		Transition.fade_to_scene("res://levels/RandomDungeon.tscn")
