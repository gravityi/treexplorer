extends VBoxContainer

func _ready():
	$"Multikill".pressed = Globals.passives_dict["multikill"]
	$"Slow".pressed = Globals.passives_dict["slow"]
	$"Explosive".pressed = Globals.passives_dict["explosive"]
	$"Double".pressed = Globals.passives_dict["double"]
	$"First".pressed = Globals.passives_dict["first"]

func update_settings():
	Globals.passives_dict["multikill"] = $"Multikill".pressed
	Globals.passives_dict["slow"] = $"Slow".pressed
	Globals.passives_dict["explosive"] = $"Explosive".pressed
	Globals.passives_dict["double"] = $"Double".pressed
	Globals.passives_dict["first"] = $"First".pressed

func _on_Exit_pressed():
	Transition.fade_to_scene("res://MainMenu.tscn")
