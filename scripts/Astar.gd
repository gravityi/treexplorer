extends Node

var all_points = {}
var cells = []

onready var astar = AStar.new()
onready var map = $"../DungeonGenerator"

func v3_to_index(v3):
	return str(int(v3.x)) + "," + str(int(v3.y)) + "," + str(int(v3.z))

func _ready():
	for cell in map.open_cells: cells.append(Vector3(cell.x, 0, cell.y))
	for cell in cells:
		var id = astar.get_available_point_id()
		astar.add_point(id, map.map_to_world(cell.x, cell.y, cell.z))
		all_points[v3_to_index(cell)] = id
	for cell in cells:
		for x in [-1, 0, 1]:
			for y in [-1, 0, 1]:
				for z in [-1, 0, 1]:
					var v3 = Vector3(x, y, z)
					if v3 == Vector3(0, 0, 0):
						continue
					if v3_to_index(v3 + cell) in all_points:
						var ind1 = all_points[v3_to_index(cell)]
						var ind2 = all_points[v3_to_index(cell + v3)]
						if !astar.are_points_connected(ind1, ind2):
							astar.connect_points(ind1, ind2, true)

func trace_path(start, end):
	var map_start = v3_to_index(map.world_to_map(start))
	var map_end = v3_to_index(map.world_to_map(end))
	var start_id = 0
	var end_id = 0
	if map_start in all_points: start_id = all_points[map_start]
	else: start_id = astar.get_closest_point(start)
	if map_end in all_points: end_id = all_points[map_end]
	else: end_id = astar.get_closest_point(end)
	return astar.get_point_path(start_id, end_id)

#func get_closest_point(target):
#	var point = astar.get_closest_point(target)
#	for key in all_points.keys():
#		if all_points[key] == point: 
#			var splitKey = key.split(",")
#			return Vector3(splitKey[0], splitKey[1], splitKey[2])
