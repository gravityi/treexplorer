extends Area


var target

func _process(delta):
	look_at(target.global_transform.origin, Vector3.UP)
	translation -= transform.basis.z * 20 * delta
	if transform.origin.distance_to(target.transform.origin) < 2:
		target.get_node("Hit").play()
		target.Player.increment_gauge(10)
		if target.health == 1: target.destroy()
		else: target.take_damage()
		queue_free()

#Enables Collision with the invisible wall when connected to area_entered
#func _on_Projectile_body_entered(body):
#	if !body.is_in_group("player") and !body.is_in_group("enemy"):
#		target.reset_word()
#		target.get_parent().add_enemy_to_list(target)
#		queue_free()
