extends KinematicBody

export var move_speed = 10

var horizontal = 0
var vertical = 0
var Projectile = preload("res://player/Projectile.tscn")
var health = 5
var gauge_value = 0

onready var PlayerAnim = $"PlayerMesh/AnimationPlayer"

func _ready():
#	Enable to keep health value between stages
#	health = Globals.health
	health = 5
	gauge_value = Globals.gauge_value
	$"../PlayerUI/Health".text = "Health: " + str(health)

func _process(delta):
	if vertical != 0 or horizontal != 0:
		PlayerAnim.play("run")
	if PlayerAnim.current_animation != "attack" and vertical == 0 and horizontal == 0:
		PlayerAnim.play("idle")
	if Input.is_action_just_pressed("ui_up"):
		vertical += 1
	if Input.is_action_just_pressed("ui_down"):
		vertical -= 1
	if Input.is_action_just_pressed("ui_left"):
		horizontal += 1
	if Input.is_action_just_pressed("ui_right"):
		horizontal -= 1

	if Input.is_action_just_released("ui_up"):
		vertical -= 1
	if Input.is_action_just_released("ui_down"):
		vertical += 1
	if Input.is_action_just_released("ui_left"):
		horizontal -= 1
	if Input.is_action_just_released("ui_right"):
		horizontal += 1
	
	#Movement and Collision Handling
	var motion = Vector3(horizontal, 0, vertical).normalized()
	if !(Input.is_action_pressed("ui_up") or Input.is_action_pressed("ui_down") or Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_right")):
		vertical = 0
		horizontal = 0
		motion = Vector3.ZERO
	else:
		motion = -Vector3(horizontal, 0, vertical).normalized()
		if transform.origin != (transform.origin - motion.rotated(Vector3.UP, deg2rad(45))):
			# THE BIG LINE (I don't really understand the concept of Quaternions, I just know how to make them work)
			transform.basis = Basis(Quat(transform.basis).slerp(Quat(transform.looking_at(transform.origin - motion.rotated(Vector3.UP, deg2rad(45)), Vector3.UP).basis), 0.1))
# warning-ignore:return_value_discarded
	move_and_collide(motion.rotated(Vector3.UP, deg2rad(45)) * delta * move_speed)
	$"../PlayerUI/ProgressBar".value = gauge_value

func shoot_projectile(target):
	look_at(Vector3(target.transform.origin.x, transform.origin.y + 3, target.transform.origin.z), Vector3.UP)
	PlayerAnim.play("attack")
	yield(get_tree().create_timer(0.8), "timeout")
	$"../Cast".play()
	var NewProjectile = Projectile.instance()
	NewProjectile.transform.origin = global_transform.origin + Vector3.UP
	NewProjectile.target = target
	get_parent().get_parent().add_child(NewProjectile)

func take_damage(amount):
	if health > 0:
		health -= amount
		Globals.health = health
	$"../PlayerUI/Health".text = "Health: " + str(health)
	$"../UIAnimationPlayer".play("damage_dealt")
	if health == 0:
		$"../MeshAnimationPlayer".play("shrink")
		yield($"../MeshAnimationPlayer", "animation_finished")
		Transition.fade_to_scene("res://levels/GameOver.tscn")

func increment_gauge(value):
	if gauge_value < 100:
		gauge_value += value
		Globals.gauge_value = gauge_value
