extends Popup

var previous_volume
var volume
var resolution
var fullscreen
var borderless
var msaa
var vsync

# Ready and process are for testing only
#func _ready():
#	call_deferred("popup_centered")

func set_button_values():
	$"GridContainer/VolumeSlider".value = volume
	if fullscreen: $"GridContainer/DisplayOptions".selected = 1
	elif borderless: $"GridContainer/DisplayOptions".selected = 2
	else: $"GridContainer/DisplayOptions".selected = 0
	for id in $"GridContainer/ResolutionOptions".get_item_count():
		if int($"GridContainer/ResolutionOptions".get_item_text(id).split("x")[0]) == resolution.x:
			$"GridContainer/ResolutionOptions".select(id)
			break
	$"GridContainer/MSAAOptions".select(msaa)
	$"GridContainer/VsyncCheck".pressed = vsync

func _on_SettingsPopup_about_to_show():
	previous_volume = db2linear(AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Master")))
	volume = db2linear(AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Master")))
	resolution = get_viewport().size
	fullscreen = OS.window_fullscreen
	borderless = OS.window_borderless
	if fullscreen: $"GridContainer/ResolutionOptions".disabled = true
	msaa = get_viewport().msaa
	vsync = OS.vsync_enabled
	set_button_values()

func _on_VolumeSlider_value_changed(value):
	volume = value
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), linear2db(volume))

func _on_DisplayOptions_item_selected(id):
	# 0 is Windowed, 1 is Fullscreen, 2 is Borderless
	match id:
		0:
			borderless = false
			fullscreen = false
		1:
			borderless = false
			fullscreen = true
		2:
			borderless = true
			fullscreen = false
	if fullscreen: $"GridContainer/ResolutionOptions".disabled = true
	else: $"GridContainer/ResolutionOptions".disabled = false

func _on_ResolutionOptions_item_selected(id):
	var selected_resolution = $"GridContainer/ResolutionOptions".get_item_text(id).split("x")
	resolution = Vector2(selected_resolution[0], selected_resolution[1])

func _on_MSAAOptions_item_selected(id):
	msaa = id

func _on_VsyncCheck_pressed():
	vsync = $"GridContainer/VsyncCheck".pressed

func _on_Reset_pressed():
	volume = 1
	resolution = Vector2(1920, 1080)
	borderless = false
	fullscreen = true
	$"GridContainer/ResolutionOptions".disabled = true
	msaa = 4
	vsync = true
	set_button_values()

func _on_Cancel_pressed():
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), linear2db(previous_volume))
	hide()

func _on_Save_pressed():
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), linear2db(volume))
	OS.window_size = resolution
	OS.window_fullscreen = fullscreen
	OS.window_borderless = borderless
	get_viewport().set_msaa(msaa)
	OS.vsync_enabled = vsync
	hide()
