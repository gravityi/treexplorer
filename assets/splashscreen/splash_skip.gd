extends VideoPlayer

export var path_to_menu = "Menu.tscn"

func _ready():
	path_to_menu = "res://" + path_to_menu

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		Transition.fade_to_scene(path_to_menu)

func _on_SplashScreenPlayer_finished():
	Transition.fade_to_scene(path_to_menu)

#Comment to remove skipping on mouse click
func _input(event):
	if event is InputEventMouseButton:
		if event.pressed and event.button_index == BUTTON_LEFT: 
			Transition.fade_to_scene(path_to_menu)
