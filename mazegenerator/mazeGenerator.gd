extends GridMap

signal can_spawn

var wall_list = []
var visited_cells = []
var player_spawn = []
var enemy_spawns = []
var level_end = []
var spawn_signal_args = []

func place_cell(x, y, id):
	set_cell_item(x, 0, y, id, 0)

func fill_maze(x_size, y_size):
	for x in x_size:
		for y in y_size:
			place_cell(x, y, 0)
			#Add Floor
			set_cell_item(x, -1, y, 1)

func generate_starting_point(x_range, y_range):
	return [randi() % x_range, randi() % y_range]

func get_neighbors(x, y):
	return [[x - 1, y], [x + 1, y], [x, y + 1], [x, y - 1]]

func get_distant_neighbor(x, y):
	return [[x - 2, y], [x + 2, y], [x, y + 2], [x, y - 2]]

func get_filled_neighbors(x, y):
	var filled_neighbors = []
	for neighbor in get_neighbors(x, y):
		if get_cell_item(neighbor[0], 0, neighbor[1]) == 0:
			filled_neighbors.append([neighbor[0], neighbor[1]])
	return filled_neighbors

func get_unvisited_neighbors(x, y):
	var neighbor_list = get_neighbors(x, y)
	for neighbor in neighbor_list:
		if neighbor in visited_cells:
			neighbor_list.erase(neighbor)
	return neighbor_list

func fill_border(x_size, y_size):
	for x in x_size + 1:
		place_cell(x, -1, 0)
		place_cell(x, y_size, 0)
	for y in y_size + 1:
		place_cell(-1, y, 0)
		place_cell(x_size, y, 0)
	place_cell(-1, -1, 0)

func erase_border(x_size, y_size):
	for x in x_size + 1:
		place_cell(x, -1, -1)
		place_cell(x, y_size, -1)
	for y in y_size + 1:
		place_cell(-1, y, -1)
		place_cell(x_size, y, -1)
	place_cell(-1, -1, -1)

func prim(x_size, y_size):
	randomize()
	var starting_point = generate_starting_point(x_size, y_size)
	visited_cells.append(starting_point)
	place_cell(starting_point[0], starting_point[1], -1)
	for neighbor in get_neighbors(starting_point[0], starting_point[1]):
		wall_list.append(neighbor)
	while !wall_list.empty():
		#yield lets you see each iteration step by step
		#yield(get_tree(), 'idle_frame')
		var random_wall = wall_list[randi() % wall_list.size()]
		var visited_neighbor_count = 0
		for neighbor in get_neighbors(random_wall[0], random_wall[1]):
			if neighbor in visited_cells:
				visited_neighbor_count += 1
		if visited_neighbor_count == 1:
			place_cell(random_wall[0], random_wall[1], -1)
			visited_cells.append(random_wall)
			for neighbor in get_unvisited_neighbors(random_wall[0], random_wall[1]):
				if get_cell_item(neighbor[0], 0, neighbor[1]) == 0:
					wall_list.append(neighbor)
			wall_list.erase(random_wall)
		else:
			wall_list.erase(random_wall)

func second_pass(x_size, y_size):
	for x in x_size:
		for y in y_size:
			if get_cell_item(x, 0, y) == 0:
				var filled_neighbors = get_filled_neighbors(x, y)
				if filled_neighbors.size() == 0:
					place_cell(x, y, -1)
				elif filled_neighbors.size() == 1:
					if get_filled_neighbors(filled_neighbors[0][0], filled_neighbors[0][1]).size() == 1:
						place_cell(filled_neighbors[0][0], filled_neighbors[0][1], -1)
						place_cell(x, y, -1)
				elif filled_neighbors.size() == 4:
					place_cell(x, y, 0)

func third_pass(x_size, y_size):
	randomize()
	for x in x_size:
		for y in y_size:
			if get_cell_item(x, 0, y) == 0:
				if rand_range(0, 1) > 0.9:
					place_cell(x, y, -1)

func fourth_pass(x_size, y_size):
	for x in x_size:
		for y in y_size:
			if get_cell_item(x, 0 , y) == 0:
				for neighbor in get_distant_neighbor(x, y):
					if get_cell_item(neighbor[0], 0, neighbor[1]) == 0 and get_cell_item((x - neighbor[0]), 0, (y - neighbor[1])) == -1:
						if (x - neighbor[0]) < 0:
							place_cell(x + 1, y, 0)
						elif(x - neighbor[0]) > 0:
							place_cell(x - 1, y, 0)
						elif (y - neighbor[1]) < 0:
							place_cell(x, y + 1, 0)
						elif (y - neighbor[1]) > 0:
							place_cell(x, y - 1, 0)

func fifth_pass(x_size, y_size):
	for x in x_size:
		for y in y_size:
			if get_cell_item(x, 0, y) == 0:
				var filled_neighbors = get_filled_neighbors(x, y)
				if filled_neighbors.size() == 1:
					if get_filled_neighbors(filled_neighbors[0][0], filled_neighbors[0][1]).size() == 1:
						place_cell(filled_neighbors[0][0], filled_neighbors[0][1], -1)
						place_cell(x, y, -1)

func random_path(x, y, x_size, y_size, direction):
	var current_position = Vector2(x, y)
	var neighbors
	for a in range(int ((x_size + y_size) / 3)):
		var next_position = [Vector2(current_position[0] + direction, current_position[1]), Vector2(current_position[0], current_position[1] + direction)][randi() % 2]
		current_position = next_position
		place_cell(current_position[0], current_position[1], -1)

func get_random_open_cell(x_size, y_size):
	var cell = [randi() % x_size, randi() % y_size]
	if get_cell_item(cell[0], 0, cell[1]) != -1:
		get_random_open_cell(x_size, y_size)
	return cell

func set_player_spawn(x_size, y_size):
	for x in x_size:
		for y in y_size:
			if get_cell_item(x, 0, y) == -1:
				return [x, y]

func set_level_end(x_size, y_size):
	for x in range(x_size, 1, -1):
		for y in range(y_size, 1, -1):
			if get_cell_item(x, 0, y) == -1:
				return [x, y]

func get_open_neighbors(x, y):
	var neighbor_count = 0
	for z in get_neighbors(x, y):
		if get_cell_item(z[0], 0, z[1]) == -1:
			neighbor_count += 1
	return neighbor_count

func get_possible_enemy_spawns(x_size, y_size):
	var possible_enemy_spawns = []
	for x in range(x_size - 3, 1, -1):
		for y in range(y_size - 3, 1, -1):
			if get_cell_item(x, 0, y) == -1 and get_open_neighbors(x, y) > 3:
				var neighbouring_enemy = false
				for neighbor in get_neighbors(x, y):
					if neighbor in possible_enemy_spawns:
						neighbouring_enemy = true
						break
				if !neighbouring_enemy:
					possible_enemy_spawns.append([x, y])
	return possible_enemy_spawns

func set_enemy_spawns(x_size, y_size, quantity):
	while enemy_spawns.size() < quantity:
		var spawn = get_possible_enemy_spawns(x_size, y_size)[randi() % get_possible_enemy_spawns(x_size, y_size).size()]
		if !spawn in enemy_spawns:
			enemy_spawns.append(spawn)

func generate_maze(x, y):
	visited_cells = []
	wall_list = []
	enemy_spawns = []
	#Standard Maze generator
	erase_border(x, y)
	fill_maze(x, y)
	prim(x, y)
	#Game specific generator
	second_pass(x, y)
	fourth_pass(x, y)
	fill_border(x, y)
	player_spawn = set_level_end(x, y)
	level_end = set_player_spawn(x, y)
	place_cell(player_spawn[0], player_spawn[1], 2)
	place_cell(level_end[0], level_end[1], 3)
#	second_pass(x, y)
	set_enemy_spawns(x, y, 10)
	var enemy_list = []
	for enemy in enemy_spawns:
		enemy_list.append(map_to_world(enemy[0], 0, enemy[1]))
	spawn_signal_args = [map_to_world(player_spawn[0], 0, player_spawn[1]), map_to_world(level_end[0], 0, level_end[1]), enemy_list]
	emit_signal("can_spawn", spawn_signal_args)

func _ready():
	connect("can_spawn", $"../EntitiesManager", "_on_map_generated")
	generate_maze(50, 50)

func _process(_delta):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
	if Input.is_action_just_pressed("generate_map"):
		generate_maze(50, 50)
