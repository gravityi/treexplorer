class_name Enemy
extends Spatial

signal word_typed

const Emitter = preload("res://enemy/EnemyEmitter.tscn")

var character
var typed_word = ""
var char_index = 0
var word = "SKELETON"
var closest = true
var health = 1
var hit = false

onready var TextManager = $"TextManager"
onready var Player = $"../../Player/PlayerBody"
onready var AnimPlayer = $"Jumper/AnimationPlayer"

func _ready():
	AnimPlayer.play_backwards("shrink")
# warning-ignore:return_value_discarded
	connect("word_typed", Player, "shoot_projectile", [self])
# warning-ignore:return_value_discarded
	connect("word_typed", get_parent(), "clean_up", [self])
	if !Globals.passives_dict["first"]: health = randi()% 2 + 1
	$"EnemyStateMachine".current_move_speed = clamp(10 - word.length(), 3, 8)
	TextManager.show_text()

func _process(_delta):
	if word == "": queue_free()
	if Globals.passives_dict["slow"] and !hit:
		if Globals.passives_dict["first"]:
			if typed_word == word[0]: $"EnemyStateMachine".current_move_speed = $"EnemyStateMachine".move_speed
			else: $"EnemyStateMachine".current_move_speed = $"EnemyStateMachine".move_speed/2
		else:
			if typed_word == "": $"EnemyStateMachine".current_move_speed = $"EnemyStateMachine".move_speed
			else: $"EnemyStateMachine".current_move_speed = $"EnemyStateMachine".move_speed/2

func _input(event):
	if event is InputEventKey:
		if !event.is_echo() and Input.is_key_pressed(event.scancode) and char_index < word.length() and closest and !AnimPlayer.current_animation == "shrink":
			character = char(event.scancode)
			if str(character) == word[char_index].to_upper():
				typed_word += character
				char_index += 1
				if Globals.passives_dict["double"]:
					char_index += 1
					typed_word += word.substr(0, char_index)
				# Execute if word completed
				if char_index >= word.length():
					emit_signal("word_typed")
				TextManager.show_text()
				$"WordAnimationPlayer".play("letterTyped")
			# Execute if user did not type correctly
			elif character.to_lower() in "abcdefghijklmnopqrstuvwxyz":
				char_index = 0
				typed_word = ""
				if Globals.passives_dict["first"]:
					char_index = 1
					typed_word = word[0]
				TextManager.show_text()

func destroy():
	AnimPlayer.play("shrink")
	$"WordAnimationPlayer".play("fullyTyped")
	create_emitter()
	char_index = 0
	typed_word = ""
	if Globals.passives_dict["explosive"]:
		for body in $"ExplosionRadius".get_overlapping_bodies():
			if body.is_in_group("enemy"):
				body.char_index += 2
				body.typed_word = body.word.substr(0, body.char_index)
				body.get_node("TextManager").show_text()
	yield(AnimPlayer, "animation_finished")
	queue_free()

func create_emitter():
	var NewEmitter = Emitter.instance()
	NewEmitter.emitting = true
	NewEmitter.transform.origin = transform.origin
	get_parent().add_child(NewEmitter)

func reset_word():
	typed_word = ""
	char_index = 0
	closest = false
	TextManager.show_text()

func change_word(new_word):
	if word != "CLEAR": get_parent().remove_enemy_from_list(self, word)
	reset_word()
	word = new_word
	get_parent().add_enemy_to_list(self)
	TextManager.show_text()
	$"EnemyStateMachine".current_move_speed = 0
	hit = true
	scale = Vector3(0.2, 0.3, 0.2) + Vector3(0.1, 0.1, 0.1) * word.length()/4
	yield(get_tree().create_timer(1), "timeout")
	hit = false
	$"EnemyStateMachine".current_move_speed = clamp(10 - word.length(), 3, 8)

func take_damage():
	reset_word()
	health -= 1
	change_word(get_parent().define_word("skeleton", true))
