extends Spatial

onready var camera = get_viewport().get_camera()
onready var label = $"Label"
onready var parent = get_parent()

func _process(_delta):
	label.rect_pivot_offset = label.rect_size/2
	label.rect_position = camera.unproject_position(global_transform.origin) - Vector2(label.rect_size.x/2, 0)

func show_text():
	$"Label".bbcode_text = "[center][color=#006c67]" + parent.word.substr(0, parent.char_index) + "[/color]" + parent.word.substr(parent.char_index, parent.word.length()) + "[/center]"

