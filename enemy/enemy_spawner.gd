extends Spatial

export var screenwipe = false
var spawned_words = []
var spawned_enemies = {}
const EnemyNode = preload("res://enemy/Enemy.tscn")
var word_data

onready var Player = $"../Player/PlayerBody"
onready var Dungeon = $"../../DungeonGenerator"

func _load_word_data():
	randomize()
	var file = File.new()
	file.open("res://data/enemywords.json", File.READ)
	word_data = parse_json(file.get_as_text())

func define_word(type_id : String, random : bool):
	var word_list = word_data[type_id]
	var random_word = word_list[randi() % word_list.size()]
	# Return random word if intended
	if random or spawned_words.empty(): return random_word 
	else:
		var possible_words = word_list.duplicate(true)
		for word in spawned_words: if word in possible_words: possible_words.erase(word)
		# Return any other word if possible
		if !possible_words.empty(): return possible_words[randi() % possible_words.size()]
		# Otherwise, return random word
		else: return random_word

func spawn_enemy(position : Vector3, type_id : String, random : bool = false):
	var NewEnemy = EnemyNode.instance()
	var word = define_word(type_id, random)
	NewEnemy.translation = position
	NewEnemy.word = word
	NewEnemy.scale = Vector3(0.2, 0.3, 0.2) + Vector3(0.1, 0.1, 0.1) * word.length()/4
	NewEnemy.get_node("EnemyStateMachine").move_speed = clamp(10 - word.length(), 3, 8)
	if Globals.passives_dict["first"]:
		NewEnemy.char_index = 1
		NewEnemy.typed_word = word[0]
	if !word in spawned_words: spawned_words.append(word)
	if !spawned_enemies.has(word): spawned_enemies[word] = [NewEnemy]
	else: spawned_enemies[word].append(NewEnemy)
	add_child(NewEnemy)

#Just a generic ascending sort function
func distance_sort(a, b):
	if a[1] < b[1]:
		return true
	return false

func sort_enemies_by_distance():
	for word in spawned_words:
		var enemy_list = []
		for enemy in spawned_enemies[word]:
			#Only appends if enemy is visible on screen
			if get_viewport().get_visible_rect().has_point(get_viewport().get_camera().unproject_position(enemy.global_transform.origin)):
				enemy_list.append([enemy, enemy.transform.origin.distance_to(Player.global_transform.origin)])
			if !enemy.closest: enemy.char_index = 0
			enemy.closest = Globals.passives_dict["multikill"]
		enemy_list.sort_custom(self, "distance_sort")
		if !enemy_list.empty(): enemy_list[0][0].closest = true

#Cleans up the data structures which contains info about spawned enemies and words.
func clean_up(caller):
	if spawned_enemies.has(caller.word):
		spawned_enemies[caller.word].erase(caller)
		if spawned_enemies[caller.word].empty():
			spawned_words.erase(caller.word)
			spawned_enemies.erase(caller.word)

func add_enemy_to_list(enemy):
	if !spawned_enemies.has(enemy.word):
		spawned_enemies[enemy.word] = []
		spawned_words.append(enemy.word)
	spawned_enemies[enemy.word].append(enemy)

func remove_enemy_from_list(enemy, word):
	if spawned_enemies.has(word): spawned_enemies[word].erase(enemy)

#Utility functions
func clear_screen():
	for word in spawned_words:
		for enemy in spawned_enemies[word]:
			enemy.destroy()
	spawned_words.clear()
	spawned_enemies.clear()

func reset_enemies():
	spawn_enemy(Vector3(5,1,5), "skeleton")
	spawn_enemy(Vector3(-5,1,5), "skeleton")
	spawn_enemy(Vector3(5,1,-5), "skeleton")
	spawn_enemy(Vector3(-5,1,-5), "skeleton")
	spawn_enemy(Vector3(-5,1,-30), "skeleton")

#Main functions
func _ready():
	if Globals.passives_dict["multikill"]: screenwipe = true
	
func _process(_delta):
	#if screenwipe: for word in spawned_words: for enemy in spawned_enemies[word]: enemy.closest = true
	if !screenwipe: sort_enemies_by_distance()
	
	if Input.is_action_just_pressed("ultimate"): ultimate()

func on_dungeon_generated(enemy_positions):
	_load_word_data()
	for enemy in enemy_positions:
		spawn_enemy(enemy, "skeleton")

func ultimate():
	if Player.gauge_value >= 50 and !Globals.passives_dict["multikill"]:
		var enemy_list = []
		for word in spawned_words:
			for enemy in spawned_enemies[word]:
				if get_viewport().get_visible_rect().has_point(get_viewport().get_camera().unproject_position(enemy.global_transform.origin)):
					enemy_list.append(enemy)
		if !enemy_list.empty():
			spawned_words.append("CLEAR")
			spawned_enemies["CLEAR"] = []
			for enemy in enemy_list:
				enemy.change_word("CLEAR")
				enemy.closest = true
				enemy.health = 1
		
		screenwipe = true
		Player.gauge_value -= 50
		Globals.gauge_value = Player.gauge_value
		yield(get_tree().create_timer(10), "timeout")
		screenwipe = false
			
