extends StateMachine

enum states {PATROL, SEARCH, ATTACK}

var target
var path
var path_idx = 1
var dealt_damage = false
var move_speed = 2
var current_move_speed = 2

onready var EnemyBody = $".."
onready var PlayerBody = $"../../../Player/PlayerBody"
onready var Astar = $"../../../../Astar"
onready var AnimPlayer = $"../Jumper/AnimationPlayer"
var space_state

func _ready():
	call_deferred("set_state", states.PATROL)
	space_state = $"..".get_world().direct_space_state

func _state_logic(delta):
	match state:
		states.PATROL:
			for body in $"../detectionRadius".get_overlapping_bodies():
				if body.is_in_group("player"):
					var result = space_state.intersect_ray($"..".global_transform.origin, body.global_transform.origin)
					if result.size() > 0: if result.collider == body: target = body
		states.SEARCH:
			if target and !AnimPlayer.current_animation == "shrink":
				var result = space_state.intersect_ray($"..".global_transform.origin, target.global_transform.origin)
				var target_position = Vector3(int(target.global_transform.origin.x), 1, int(target.global_transform.origin.z))
				var self_position = (Vector3(int($"..".global_transform.origin.x), 1, int($"..".global_transform.origin.z)))
				if result.size() > 0: 
					if result.collider == target:
						path_idx = 1
						$"..".look_at(Vector3(target_position.x, $"..".transform.origin.y , target_position.z), Vector3.UP)
						$"..".move_and_slide((target_position - self_position).normalized() * current_move_speed, Vector3.UP)
					else:
						path = Astar.trace_path(self_position, target_position)
						if path_idx < path.size():
							var move_vector = path[path_idx] - self_position
							if move_vector.length() < 0.1:
								path_idx += 1
							else:
								if path_idx + 1 < path.size(): $"..".look_at(Vector3(path[path_idx + 1].x, $"..".global_transform.origin.y, path[path_idx + 1].z), Vector3.UP)
								$"..".move_and_slide(move_vector.normalized() * current_move_speed, Vector3.UP)

func _get_transition(delta):
	match state:
		states.PATROL:
			if target: return states.SEARCH
		states.SEARCH:
			for body in $"../AttackArea".get_overlapping_bodies():
				if body.is_in_group("player"): return states.ATTACK

func _enter_state(new_state, old_state):
	match new_state:
		states.PATROL:
			AnimPlayer.play("JumperIdle")
		states.SEARCH:
			AnimPlayer.play("JumperRun")
		states.ATTACK:
			if target.health > 0: 
				AnimPlayer.play("JumperAttack")
				$"../Attack".play()

func _exit_state(old_state, new_state):
	pass

func deal_damage():
	for body in $"../AttackArea".get_overlapping_bodies():
		if body == target: target.take_damage(1)
