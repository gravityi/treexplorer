extends GridMap

signal map_generated

enum room_types { EMPTY, ENEMY }

var origin_cells = []
var starting_room_cells = []
var connections = []
var enemy_positions = []
var open_cells = []

onready var EnemySpawner = $"../EntitiesManager/EnemySpawner"

func get_neighbors(cell_position, x_range, y_range):
	var neighbors = []
	for x in range(-x_range, x_range + 1):
		for y in range(-y_range, y_range + 1):
			neighbors.append(Vector2(cell_position.x + x, cell_position.y + y))
	return neighbors

func get_taxicab_distance(cell_1, cell_2):
	var x_diff = abs(cell_1.x - cell_2.x)
	var y_diff = abs(cell_1.y - cell_2.y)
	return x_diff + y_diff

func get_valid_origin_cell(x_bounds, y_bounds, min_distance):
	var cell_position = Vector2(randi() % x_bounds * 2 - x_bounds, randi() % y_bounds * 2 - y_bounds)
	for origin in origin_cells:
		if get_taxicab_distance(origin, cell_position) < min_distance:
			return get_valid_origin_cell(x_bounds, y_bounds, min_distance)
	return cell_position

func create_origin_cell(x_bounds, y_bounds, cell_idx):
	var cell_position = get_valid_origin_cell(x_bounds, y_bounds, 15)
	set_cell_item(cell_position.x, 0, cell_position.y, cell_idx)
	origin_cells.append(cell_position)

func get_valid_enemy_position(possible_positions, placed_enemies):
	var enemy_position = possible_positions[randi() % possible_positions.size()]
	if enemy_position in placed_enemies:
		return get_valid_enemy_position(possible_positions, placed_enemies)
	return enemy_position

func set_enemy_positions(origin, room_size, quantity):
	var placed_enemies = []
	var possible_positions = get_neighbors(origin, room_size.x, room_size.y)
	for _enemy in range(quantity):
		placed_enemies.append(get_valid_enemy_position(possible_positions, placed_enemies))
	return placed_enemies

func make_room(origin, min_size, max_size, room_type):
	var size = Vector2(randi() % max_size + min_size, randi() % max_size + min_size)
	for neighbor in get_neighbors(origin, size.x, size.y):
		if !neighbor in origin_cells:
			set_cell_item(neighbor.x, 0, neighbor.y, 1)
	if room_type == room_types.ENEMY:
		for enemy in set_enemy_positions(origin, size, randi() % 3 + 2):
			enemy_positions.append(map_to_world(enemy.x, 1, enemy.y))

func place_path_cell(x, y, cell_idx):
	if !Vector2(x, y) in origin_cells:
		set_cell_item(x, 0, y, cell_idx)

func make_path(pos1, pos2, cell_idx):
	var x_diff = sign(pos2.x - pos1.x)
	var y_diff = sign(pos2.y - pos1.y)
	if x_diff == 0: x_diff = pow(-1.0, randi() % 2)
	if y_diff == 0: y_diff = pow(-1.0, randi() % 2)
	var x_y = pos1
	var y_x = pos2
	if (randi() % 2) > 0:
		x_y = pos2
		y_x = pos1
	for x in range(pos1.x, pos2.x, x_diff):
		place_path_cell(x, x_y.y, cell_idx)
		place_path_cell(x, x_y.y + y_diff, cell_idx)
		place_path_cell(x, x_y.y - y_diff, cell_idx)
	for y in range(pos1.y, pos2.y, y_diff):
		place_path_cell(y_x.x, y, cell_idx)
		place_path_cell(y_x.x + x_diff, y, cell_idx)
		place_path_cell(y_x.x - x_diff, y, cell_idx)

func raise_walls(x_bounds, y_bounds):
	for x in range(-x_bounds, x_bounds):
		for y in range(-y_bounds, y_bounds):
			if get_cell_item(x, 0, y) < 0:
				set_cell_item(x, 1, y, 0)
			elif get_cell_item(x, 0, y) > 0:
				open_cells.append(Vector2(x, y))

func generate_dungeon():
	randomize()
	clear()
	origin_cells = []
	open_cells = []
	for _c in range(randi() % 5 + 5):
		create_origin_cell(30, 30, 1)
	for cell_idx in range(0, origin_cells.size() - 1):
		make_path(origin_cells[cell_idx], origin_cells[cell_idx + 1], 1)
		connections.append([cell_idx, cell_idx + 1])
	make_room(origin_cells[0], 3, 5, room_types.EMPTY)
	make_room(origin_cells[origin_cells.size() - 1], 3, 5, room_types.EMPTY)
	for cell in origin_cells.slice(1, origin_cells.size() - 2):
		make_room(cell, 3, 8, room_types.ENEMY)
	raise_walls(50, 50)
	emit_signal("map_generated", map_to_world(origin_cells[0].x, 1, origin_cells[0].y), map_to_world(origin_cells[origin_cells.size() - 1].x, 1, origin_cells[origin_cells.size() - 1].y), enemy_positions)

func _ready():
# warning-ignore:return_value_discarded
#	connect("dungeon_generated", EnemySpawner, "on_dungeon_generated", enemy_positions)
	connect("map_generated", $"../EntitiesManager", "_on_map_generated")
	generate_dungeon()
